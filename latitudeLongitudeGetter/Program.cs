﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading;
using Newtonsoft.Json.Linq;
using System.Data;

namespace latitudeLongitudeGetter
{
    class Program
    {
        static void Main(string[] args)
        {

           apiCallAsync().Wait();

        }
        public static async Task apiCallAsync()
        {
            string path = "server=198.143.98.122;database=veritas;User Id=sa;Password=NCC1701E";
            string sSQL = "select * from veritas.dbo.servicecenter where Latitude is NULL AND Longitude is NULL";
            string countSQL = "select count(*) as Count from ServiceCenter where Latitude is not NULL AND Longitude is not NULL";

            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            DataTable dt = dBO.dboInportInformation(sSQL);

            int totalCount;
            DataTable countTable = dBO.dboInportInformation(countSQL);
            totalCount = (int)countTable.Rows[0]["Count"];
           
            foreach (DataRow dr in dt.Rows)
            {
                int resultIterator = 0;
                int locationIterator = 0;

                if (totalCount >= 20000)
                {
                    goto maxedCalls;
                }
                else
                {
                    if (dr["Addr1"].ToString() != "" && dr["Zip"].ToString() != "" && dr["State"].ToString() != "" && dr["city"].ToString() != "")
                    {
                        string address = dr["Addr1"].ToString();
                        string city = dr["City"].ToString();
                        string state = dr["State"].ToString();
                        string zip = dr["Zip"].ToString();
                        string lat = "";
                        string lon = "";
                        if (address.IndexOfAny("*#".ToCharArray()) == -1)
                        {
                            try
                            {
                                var client = new HttpClient();
                                var response = await client.GetStringAsync("http://www.mapquestapi.com/geocoding/v1/address?key=tp8qnYL9fgZfGba0bwg6YepZGDv0KlE7&location=" + address + ", " + city + ", " + state + ", " + zip);

                                var jobject = JObject.Parse(response);
                                var results = jobject["results"];
                                foreach (var result in results)
                                {
                                    if (resultIterator == 0)
                                    {
                                        var locations = result["locations"];
                                        foreach (var location in locations)
                                        {
                                            if (locationIterator == 0)
                                            {
                                                //var pair = location["latLng"];
                                                //Console.WriteLine("Lat: {0}, Lng: {1}", pair["lat"], pair["lng"]);
                                                lat = location["latLng"]["lat"].ToString();
                                                lon = location["latLng"]["lng"].ToString();
                                            }
                                            locationIterator = locationIterator + 1;
                                        }
                                    }
                                    resultIterator = resultIterator + 1;
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                            }
                            if (address.Contains("'"))
                            {
                                address = address.Replace("'", "''");
                            }
                            sSQL = "update veritas.dbo.servicecenter " +
                                    "set Latitude = " + lat + ", Longitude = " + lon + " " +
                                    "where Addr1 = '" + address + "' AND " +
                                    "Zip = '" + zip + "' ";
                            dBO.dboAlterDBOUnsafe(sSQL);
                            totalCount = totalCount + 1;
                        }
                    }
                }
            }
            maxedCalls:;
            dBO.dboClose();
        }
    }
}
